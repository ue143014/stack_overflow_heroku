Rails.application.routes.draw do
  
  resources :users
  resources :questions
  resources :answers
  resources :comments
  resources :votes
  resources :tags

  post "/login", to: "users#log_in"
  post "/search", to: "questions#search"

  # resources :users do
  #   resources :questions do
  #     resources :answers
  #   end
  # end

end