module AnswersModule
	
	class AnswerManager
		
		def initialize(params, current_user= nil)
			self.params = params
			self.current_user = current_user
		end

		def update_answer_details!
			return updated_answer! if authenticated_user?
		end

		def remove_answer?
			return destroy_answer? if authenticated_user?
		end

		private		
		attr_accessor :params, :current_user

		def authenticated_user?
			@answer = Answer.find(params[:answer_id])
			return true if current_user.is_admin || (@answer.user_id == current_user.id)
		end

		def updated_answer!
			@answer.value = params[:answer]
			@answer.save!
			@answer
		end

		def destroy_answer?
			return true if @answer.destroy!
		end


	end
end
