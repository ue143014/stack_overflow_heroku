class CreateVotes < ActiveRecord::Migration[5.1]
  def change
    create_table :votes do |t|
      t.integer :weight, default: 0
      t.references :voteable, polymorphic: true, index: true
 
      t.belongs_to :user
      t.timestamps
    end
  end
end
