class CommentSerializer < ApplicationSerializer

	attributes :id, :value
	belongs_to :user
	# attributes :value

	def value
		return object.value if object.status == "active"		
	end

end