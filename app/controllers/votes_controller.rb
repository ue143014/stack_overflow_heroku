class VotesController < ApplicationController	
	def create
		@vote = VotesModule::VoteCreator.new(user_params, @current_user).create_vote!
		render json: @vote
	end

	private

	def user_params
		params.permit(:user_id, :voteable_id, :voteable_type, :vote)
	end
end
	