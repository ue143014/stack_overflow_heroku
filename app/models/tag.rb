class Tag < ApplicationRecord
	has_and_belongs_to_many :questions

	before_save { self.value = value.downcase }
	validates :value, presence: true, length: { maximum: 15 } , uniqueness: { case_sensitive: false }

	def self.search(search_term)
		self.where("value ilike ?", "%#{search_term}%")
	end
end
