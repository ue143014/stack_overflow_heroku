class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :password_digest
      t.boolean :is_admin, default: false
      t.integer  :status, default: 1
      t.integer :reputation, default: 0

      t.timestamps
    end
  end
end