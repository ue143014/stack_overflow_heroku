module TagsModule
	class TagsCreator
		
		def initialize(params, current_user)
			self.params = params
			self.current_user = current_user
		end

		def add_tag!
			return insert_tag_value? if validate_user?
		end

		private
		attr_accessor :params, :current_user

		def validate_user?
			return true if current_user.is_admin?
		end

		def insert_tag_value?
			all_tags = []
			tags = params[:tags]
			tags.length.times do |i|
				all_tags << Tag.new(value: tags[i])
			end
			Tag.import all_tags, :validate => true
			return true
		end

	end
end