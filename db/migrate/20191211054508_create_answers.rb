class CreateAnswers < ActiveRecord::Migration[5.1]
  def change
    create_table :answers do |t|
      t.text :value
      t.integer :status, default: 1
      t.belongs_to :user
      t.belongs_to :question

      t.timestamps
    end
  end
end
