module QuestionsModule
	class QuestionCreator
		
		def initialize(params)
			self.params = params
		end
		
		def post_question!
			@question = Question.create!( "user_id": params[:user_id],
																		"value": params[:question] )
			@tag = Tag.where(value: [params[:tags]])
			@question.tags << @tag unless @question.tags.include?(@tag)
			return @question
		end

		private
		attr_accessor :params

	end
end