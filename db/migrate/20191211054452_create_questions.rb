class CreateQuestions < ActiveRecord::Migration[5.1]
  def change
    create_table :questions do |t|
      t.text :value
      t.integer :status, default: 1
      t.belongs_to :user

      t.timestamps
    end
  end
end
