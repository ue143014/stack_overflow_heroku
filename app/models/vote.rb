class Vote < ApplicationRecord

    belongs_to :user
    belongs_to :voteable, polymorphic: true

    enum weight: { "upvote": 1 , "downvote": -1 , "neutral": 0}
    validates :weight, presence: true, inclusion: { in: %w(upvote downvote neutral) }
end
