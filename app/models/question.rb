class Question < ApplicationRecord

    belongs_to :user
    has_many :answers
    has_and_belongs_to_many :tags
    has_many :votes, as: :voteable
    has_many :comments, as: :commentable

    enum status: { inactive: 0, active: 1, inreview: 2, rejected: 3 }
    before_save { self.value = value.downcase }
    validates :status, presence: true, inclusion: { in: %w(active inactive inreview rejected ) }
    validates :value, presence: true, length: { maximum: 500 }, uniqueness: { case_sensitive: false }

    def destroy!
		  update_attribute(:status, "inactive")
	  end

end
