module UserModule
	class UserCreator

		def initialize(params)
			self.params = params
		end

		def create_user!
			@user = User.create!( "name": params[:name],
														"email": params[:email],
														"password": params[:password] )
			return @user
		end

		private
		attr_accessor :params

	end
end