class UsersController < ApplicationController
	skip_before_action :validate_user?, only: [:create, :log_in]

	def index
		render json: @current_user
	end

	def create
		@user = UserModule::UserCreator.new(user_params).create_user!
		render json: @user
	end

	def log_in
		@user = UserModule::UserManager.new(user_params, @current_user).user_login!
		render json: @user
	end

	def update
		@user = UserModule::UserManager.new(user_params, @current_user).update_user_details!
		render json: @user
	end

	def destroy
		flag = UserModule::UserManager.new(user_params, @current_user).remove_user?
		flag ? json_response("User deleted sussessfully !!") : json_response("You are not an authenticated user.")
	end

	private 

	def user_params
		params.permit(:name, :email, :password, :password_confirmation, :reputation, :status, :user_id, :remove_user_id)
	end

end