class QuestionsController < ApplicationController
	
	skip_before_action :validate_user?, only: [:search]
	
	def index
		@question = Question.find(question_params[:question_id])
		# paginated_data = @questions.page(1).per_page(2)
		render json: @question
		# render json: paginated_data
	end

	def create
		@question = QuestionsModule::QuestionCreator.new(question_params).post_question!
		render json: @question
	end

	def search
		@questions = QuestionsModule::QuestionsManager.new(question_params).find_questions!
		render json: @questions
	end

	def update
		@question = QuestionsModule::QuestionsManager.new(question_params,@current_user).update_question_details!
		render json: @question
	end

	def destroy
		flag = QuestionsModule::QuestionsManager.new(question_params,@current_user).remove_question?
		flag ? json_response("Successfully Removed !!") : json_response("You are not an authenticate user !!")
	end
		
	private
	def question_params
		params.permit(:user_id, :question, :question_id, :tags => [])
	end

end