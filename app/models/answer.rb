class Answer < ApplicationRecord

    belongs_to :user
    belongs_to :question
    has_many :votes, as: :voteable
    has_many :comments, as: :commentable

    enum status: { inactive: 0, active: 1, inreview: 2, rejected: 3 }
    before_save { self.value = value.downcase }
    validates :status, presence: true, inclusion: { in: %w(active inreview rejected inactive) }
    validates :value, presence: true, length: {maximum: 500 }, uniqueness: { case_sensitive: false }

		def destroy!
			update_attribute(:status, "inactive")
    end

end
