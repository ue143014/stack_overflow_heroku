class Comment < ApplicationRecord

    belongs_to :user
    belongs_to :commentable, polymorphic: true

    enum status: {inactive: 0, active: 1, inreview: 2, rejected: 3}
    before_save { self.value = value.downcase }
    validates :value, presence: true, length: { maximum: 250 }
    validates :status, presence: true, inclusion: { in: %w(active inactive inreview rejected ) }

    def destroy!
			update_attribute(:status, "inactive")
    end

end
