module Response
	
	def json_response(object, status = :ok, message = :error)
		render json: object, status: status, message: message
	end
	
end