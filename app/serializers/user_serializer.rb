class UserSerializer < ApplicationSerializer

	attributes :id, :name, :email, :reputation, :status, :is_admin
	
	has_many :questions
	has_many :answers
	
end
