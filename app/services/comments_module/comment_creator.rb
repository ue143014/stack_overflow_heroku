module CommentsModule
	class CommentCreator
		
		def initialize(params, current_user= "nil")
			self.params =params
			self.current_user = current_user
		end

		def post_comment!
			@comment = Comment.create!( user_id: params[:user_id],
																	commentable_type: params[:commentable_type],
																	commentable_id: params[:commentable_id],
																	value: params[:comment]	)
			return @comment
		end

		private
		attr_accessor :params, :current_user

	end
end
