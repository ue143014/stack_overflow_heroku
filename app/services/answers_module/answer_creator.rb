module AnswersModule
	class AnswerCreator
		
		def initialize(params, current_user= nil)
			self.params = params
			self.current_user = current_user
		end

		def post_answer!
			return post! if validated?
		end

		private
		attr_accessor :params, :current_user

		def validated?
			@question = Question.find(params[:question_id])
			return true if current_user.id != @question.user_id 
		end

		def post!
			@answer = Answer.create!( "user_id": params[:user_id],
																"value": params[:answer],
																"question_id": params[:question_id] )
			return @answer
		end

	end
end