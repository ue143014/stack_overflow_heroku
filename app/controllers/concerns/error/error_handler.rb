module Error
	module ErrorHandler
		include Response
		
		def self.included(clazz)
			clazz.class_eval do
				rescue_from ActiveRecord::RecordNotFound do |e|
					json_response(:record_not_found, 404, e.to_s)
				end
				
				rescue_from NoMethodError do |e|
					json_response(:no_method_error, 500, e.to_s)
				end

				rescue_from AbstractController::ActionNotFound do |e|
					json_response(:action_not_found, 500, e.to_s)
				end
				
				rescue_from AbstractController::DoubleRenderError do |e|
					json_response(:double_render_error, 500, e.to_s)
				end
				# rescue_from StandardError do |e|
				# 	json_response(:standard_error, 500, e.to_s)
				# end
				
			end
		end

	end
end