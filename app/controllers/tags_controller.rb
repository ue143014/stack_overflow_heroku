# Single place error handling - Aprox Done
# Exception => e this will handle all standard error, rescue specific error. - Aprox Done
# Method naming convention. - Done
# search api is doable from index after applying filters - Done
# Validation of actions should in service layer - Done - Single- Bulk Create

class TagsController < ApplicationController

	skip_before_action :validate_user?, only: [:index]
	# Just testing pagination
	def index
		tags = Tag.search(tag_params[:tag])
		# paginated_data = tags.page(1).per_page(3)
		render json: tags
		# render json: paginated_data, meta: pagination(paginated_data, 3)
	end

	def create
		flag = TagsModule::TagsCreator.new(tag_params, @current_user).add_tag!
		flag ? json_response("Tags Created Successfully !! ") : json_response("Not Authorized User")
	end

	def destory
		flag = TagsModule::TagsManager.new(tag_params, @current_user).remove_tags?
		flag ? json_response("Tags Removed Successfully !! ") : json_response("You are not an authenticate user !!")
	end

	private

	def tag_params
		params.permit(:user_id, :tag, :tags => [])
	end

end