class CommentsController < ApplicationController
				
	def index
		@comment = Comment.find(comment_params[:commentable_id])
		render json: @comment
	end


	def create
		@comment = CommentsModule::CommentCreator.new(comment_params).post_comment!
		render json: @comment
	end

	def update
		@comment = CommentsModule::CommentManager.new(comment_params, @current_user).update_comment_details!
		render json: @comment
	end

	def destroy
		flag = CommentsModule::CommentManager.new(comment_params, @current_user).remove_comments?
		flag ? json_response("Successfully Removed !!") : json_response("You are not an authenticate user !!")
	end

	private

	def comment_params
		params.permit(:user_id, :commentable_type, :commentable_id, :comment, :comment_id, :id)
	end

end