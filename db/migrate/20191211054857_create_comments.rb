class CreateComments < ActiveRecord::Migration[5.1]
  def change
    create_table :comments do |t| 
      t.integer :status, default: 1
      t.text :value

      t.references :commentable, polymorphic: true, index: true
      
      t.belongs_to :user
      t.timestamps
    end
  end
end
