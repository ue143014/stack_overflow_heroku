class AnswersController < ApplicationController

	def index
		@answer = Answer.find(answer_params[:answer_id])
		render json: @answer
	end
	
	def create
		@answer = AnswersModule::AnswerCreator.new(answer_params, @current_user).post_answer!
		render json: @answer
	end

	def update
		@answer = AnswersModule::AnswerManager.new(answer_params, @current_user).update_answer_details!
		render json: @answer
	end

	def destroy
		flag = AnswersModule::AnswerManager.new(answer_params, @current_user).remove_answer?
		flag ? json_response("Successfully Removed !!") : json_response("You are not an authenticate user")
	end

	private
	
	def answer_params
		params.permit(:user_id, :question_id, :answer, :answer_id)
	end

end		