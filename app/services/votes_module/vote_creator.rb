module VotesModule
  class VoteCreator
  
    def initialize(params, current_user)
      self.params = params
      self.current_user = current_user
    end

    def create_vote!
      return create! if authenticate_user?
    end

    private
    attr_accessor :params, :current_user

    def authenticate_user?
      flag = false
      if (params[:voteable_type].downcase == 'question' && Question.find(params[:voteable_id]).user_id != current_user.id)
        flag = true      
      elsif (params[:voteable_type].downcase == 'answer' && Answer.find(params[:voteable_id]).user_id != current_user.id)
        flag = true
      end
      return flag
    end

    def create!
      flag = true
      @vote = Vote.find_or_create_by!(user_id: params[:user_id],
                                      voteable_type: params[:voteable_type],
                                      voteable_id: params[:voteable_id] do |vote_value| 
                                        flag = false
                                        vote_value.weight = params[:vote]
                                      end)
      if flag                
        @vote.update_attribute(:weight,params[:vote])
      end
      return @vote
    end

	end
end