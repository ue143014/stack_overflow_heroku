class QuestionSerializer < ApplicationSerializer

	attributes :id, :value

	has_many :answers
	has_many :tags
	belongs_to :user
	has_many :comments
	has_many :votes

	def votes
		object.votes.select(:weight).group(:weight).count
	end

	def value
		return object.value if object.status == "active"
	end

end